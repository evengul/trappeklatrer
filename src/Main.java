//Main-klasse som inneholder robot-objektet, og kj�rer dette
class Main{
	//Startpunktet i programmet v�rt
	public static void main(String[] args) throws Exception{
		Robot climber = new Robot(100);							//Et objekt av typen Robot med hastighet = 100
		climber.drive();										//Kj�r roboten, og g� opp et trinn om n�dvendig.
	}
}