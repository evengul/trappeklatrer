import lejos.hardware.motor.*;							//Motorstyring
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.port.Port;						//Lesing av sensorporter
import lejos.hardware.Brick;							//Basiskommunikasjon med EV3-"brikken"
import lejos.hardware.BrickFinder;						//Finn EV3-"Brikken"
import lejos.hardware.sensor.EV3UltrasonicSensor;		//Bruk av ultralydsensor
import lejos.robotics.SampleProvider;					//F� informasjon fra sensorer


//Klasse for � kontrollere heisen(e)
class Elevator{

	NXTRegulatedMotor elevatorMotor;					//Abstraksjon for heismotor
	NXTRegulatedMotor forwardMotor;						//Abstraksjon for fremdriftsmotor
	static private int elevatorSpeed;					//Hastighet

	Brick brick = BrickFinder.getDefault();				//Finn EV3-brikken vi skal bruke
	Port distanceFrontPort_S4 = brick.getPort("S4");	//Definer sensorport 4 til bruk

	EV3UltrasonicSensor distanceFrontSensor = new EV3UltrasonicSensor(distanceFrontPort_S4);	//Sett sensorport 4 til � bruke en avstandssensor
	SampleProvider distanceFrontReader = distanceFrontSensor.getDistanceMode();					//Et objekt som henter avstanden fra sensoren
	float[] distanceFrontSample = new float[distanceFrontReader.sampleSize()];					//En variabel for � holde p� avstanden fra sensoren


	//Definer alle heisene, og hastigheten
	public Elevator(NXTRegulatedMotor elevate, NXTRegulatedMotor forward, int elevatorSpeed){
		this.elevatorMotor = elevate;
		this.forwardMotor = forward;
		this.elevatorSpeed = elevatorSpeed;
	}

	//Hold heisene p� samme posisjon
	public void hold(){
		this.elevatorMotor.setSpeed(1);								//Sett hastigheten p� heisen til 1
		this.elevatorMotor.forward();								//Kj�r fremre heis
	}

	//Kj�r heisen opp
	public void up(){
			this.elevatorMotor.setSpeed(this.elevatorSpeed);		//Sett hastigheten p� heisen
			this.elevatorMotor.backward();							//Kj�r heisen bakover (dette er opp i denne konfigurasjonen av roboten).
	}

	//F� heisen over en trapp med ukjent h�yde
	public void firstUp(){
		distanceFrontReader.fetchSample(distanceFrontSample, 0);	//Oppdatere avstandssensorverdien
		while(distanceFrontSample[0] < 0.1){						//Mens det er kort avstand foran heisen
			up();													//Kj�r opp
		}
		try{
			Thread.sleep(2600);										//Kj�r i 2600 ekstra millisekunder for � f� den delen av heisen som er under avstandssensoren over trinnet
		}
		catch (InterruptedException ie){}
		this.elevatorMotor.stop();									//Slutt � heise
	}

	//Kj�r heisen ned
	public void down(){
		this.elevatorMotor.setSpeed(this.elevatorSpeed);			//Sett hastigheten
		this.forwardMotor.setSpeed(this.elevatorSpeed / 10);		//Fremdrift bak for � sikre at vi ikke glir p� grunn av vekt
		this.elevatorMotor.forward();								//Kj�r motoren fremover (dette kj�rer heisen ned i denne konfigurasjonen av roboten).
		this.forwardMotor.backward();								//Kj�r fremdrift p� bakre hjul
		try{
			Thread.sleep(5000);										//I 5000 millisekunder for � komme til topps
		}
		catch (InterruptedException ie){}
	}

	//Kj�r heisen fremover
	public void forward(int forwardSpeed){
		this.forwardMotor.setSpeed(forwardSpeed);					//Sett hastigheten for � kj�re fremover
		this.forwardMotor.backward();								//Kj�r fremover
	}

	//Kj�r heisen bakover
	public void backward(int backwardSpeed){
		this.forwardMotor.setSpeed(backwardSpeed);					//Sett hastigheten for � kj�re bakover
		this.forwardMotor.forward();								//Kj�r bakover
	}

	//Slutt � kj�re fremover / bakover
	public void stop(){
		this.forwardMotor.stop();
	}
}
