import lejos.hardware.motor.*;							//Motorstyring

//Klasse for � styre kroppen
class Body{

	static private int speed;							//Hastighet for motoren

	NXTRegulatedMotor motor;							//Abstraksjon for motoren

	public Body(NXTRegulatedMotor motor, int speed){
		this.motor = motor;								//Motoren settes fra Robot-klassen
		this.speed = speed;								//Hastigheten settes fra Robot-klassen
	}

	//Kj�r forover
	public void forward(){
		motor.setSpeed(speed);							//Sett hastigheten for fremdrift p� hovedplattform
		motor.forward();								//Fremdrift for hovedplattform
	}

	//Kj�r bakover
	public void backward() {
		motor.setSpeed(speed);							//Sett hastigheten for fremdrift p� hovedplattform
		motor.backward();								//Fremdrift for hovedplattform
	}

	//Stopp
	public void stop(){
		motor.stop();									//Stopp fremdrift for hovedplattform
	}
}