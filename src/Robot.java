/*

Dette programmet kj�rer en robot opp trapper med f�lgende konfigurasjon (Definisjoner f�lger lenger ned:
Sensorport 2: 	Trykksensor
Sensorport 4: 	Avstandssensor

Motor A: 		Hovedplattformens fremdrift
Motor B:		Fremdrift p� bakre heis
Motor C:		Heising av fremre heis
Motor D:		Heising av bakre heis

Roboten har en hovedplattform, og en heis i hver ende, som kj�res med motorer som driver belter via et tannhjul hver.
Gjennomf�ring:
1.	Roboten kj�rer fremover til den treffer noe med trykksensoren
2.	Roboten kj�rer litt bakover 														(tidsstyrt)
3.	Fremre heis blir kj�rt opp 															(avstandssensor)
4.	Roboten kj�res frem til fremre heis er opp� neste trinn 							(tidsstyrt)
5. 	Fremre heis kj�res ned, samtidig som bakre heis kj�res opp for � heise hele roboten.(tidsstyrt)
6.	Roboten kj�res frem til hovedplattformen er opp� nestetrinn							(tidsstyrt)
7. 	Bakre heis kj�res opp																(tidsstyrt)
8.	Gjenta steg 1-7																		(while(true)-l�kke)

Skrevet av Gultvedt,Heggelund, Hollum, Kalleberg & Koch.

Versjon 2

(C) 2018

*/

//Importere n�dvendige biblioteker
import lejos.hardware.motor.*;							//Motorstyring
import lejos.hardware.port.Port;						//Lesing av sensorporter
import lejos.hardware.Brick;							//Basiskommunikasjon med EV3-"brikken"
import lejos.hardware.BrickFinder;						//Finn EV3-"Brikken"
import lejos.hardware.sensor.EV3TouchSensor;			//Bruk av trykksensor
import lejos.robotics.SampleProvider;					//F� informasjon fra sensorer

//Klassen som skal styre roboten
class Robot{

	static private int speed;																	//Teknisk hastighet som brukes for alle motorer

	Brick brick = BrickFinder.getDefault();														//Finn EV3-brikken vi skal bruke
	Port pushFrontPort_S2 = brick.getPort("S2");												//Definer sensorport 2 til bruk

	SampleProvider touchFrontReader = new EV3TouchSensor(pushFrontPort_S2);						//Sett sensorport 2 til � bruke en trykksensor
	float[] touchFrontSample = new float[touchFrontReader.sampleSize()];						//En variabel for � holde p� avstanden fra sensoren. Kun av/p�.

	Elevator front;		//Deklarer fremre heis
	Elevator back;		//Deklarer bakre heis
	Body body;			//Deklarer kroppen


	//Konstrukt�r som setter standard hastighet for alle motorer
	public Robot(int speed){
		this.speed = speed;										//Hastigheten settes fra parameter

		front = new Elevator(Motor.C, Motor.B, speed);			//Fronten har motor C som heismotor, og B som drivkraft. Denne settes til B for � kunne kj�res, men i praksis finnes den ikke.
		back = new Elevator(Motor.D, Motor.B, speed);			//Bakenden har motor D som heismotor, og B som drivkraft.
		body = new Body(Motor.A, speed);						//Kroppen har motor A som drivkraft

		//Alle hastigheter settes f�r noe annet gj�res
		Motor.A.setSpeed(speed);
		Motor.B.setSpeed(speed);
		Motor.C.setSpeed(speed);
		Motor.D.setSpeed(speed);
	}

	//Metode for � kj�re forover
	private void driveForward(){
		body.forward();				//Kj�r hovedplattformen fremover
		back.forward(speed);		//Kj�r bakre heis fremover
	}

	//Metode for � kj�re bakover
	private void driveBackward() {
		body.backward();			//Kj�r
		back.backward(speed);		//Kj�r bakre heis bakover
	}

	//Metode for � stoppe fremdrift
	private void stopDriving(){
		body.stop();				//Stopp hovedplattformens fremdrift
		back.stop();				//Stopp bakre heis fremdrift
	}

	//Hold heisene p� samme posisjon
	private void holdElevators(){
		back.hold();				//Hold bakre heis
		front.hold();				//Hold fremre heis
	}

	//Er trykksensoren aktivert?
	private boolean touching(){
			touchFrontReader.fetchSample(touchFrontSample, 0);			//Oppdatere trykksensorverdien
			if (touchFrontSample[0] > 0){								//Hvis den aktiveres:
				return true;											//True
			}
			else{														//Hvis ikke
				return false;											//False
			}
	}

	//Kj�r bakover etter � truffet en vegg/trappetrinn
	private void backAfterHittingWall(){
		stopDriving();													//Slutt � kj�re
		driveBackward();												//Kj�r bakover
		try{
			Thread.sleep(400);											//I 400 ms
		}
		catch (InterruptedException ie) {}
		stopDriving();													//Stopp fremdrift
	}

	//Kj�r plattformen inntil neste trinn
	private void bodyNextToWall(){
		driveForward();													//Kj�r fremover
		try{
			Thread.sleep(2500); 										//I 2500 millisekunder for � komme inntil
		}
		catch (InterruptedException ie){}
		stopDriving();													//Stopp fremdrift
	}

	//Kj�r heis foran ned, og bak opp samtidig. Like lenge som vi regnet ut at vi trenger.
	private void elevateBody(){
		front.down();													//Kj�r fremre heis ned
		back.down();													//Kj�r bakre heis ned
		back.forward(speed / 10);										//Kj�r fremdrift p� bakre heis for � sikre at denne heisen ikke glir bakover
		try{
			Thread.sleep(5000);											//I 5000 millisekunder for � komme til topps
		}
		catch (InterruptedException ie){}
		holdElevators();												//Hold heisene p� plass. Stopper vi n�, s� glir de ned igjen.
	}

	//Kj�r fremover med hovedplattformen i luften
	private void driveWithBodyInAir(){
		driveForward();													//Kj�r fremover
		back.forward(speed + speed / 10);								//Litt raskere fremdrift p� bakre heis for ikke � gli
		try{
			Thread.sleep(3500);											//I 3500 millisekunder
		}
		catch (InterruptedException ie){}
		stopDriving();													//Slutt � kj�re fremover
	}

	//Trekk opp bakre heis
	private void elevatorBackUp(){
		front.up();														//Kj�r fremre heis opp
		back.up();														//Kj�r bakre heis opp
		try{
			Thread.sleep(200);											//I 200 millisekunder
			front.hold();												//Stopp og hold fremre heis
			Thread.sleep(5000);											//Kj�r i 5000 millisekunder til
		}
		catch (InterruptedException ie){}
		back.hold();													//Stopp og hold bakre heis
		stopDriving();													//Stopp fremdrift
	}

	//Kj�r roboten fremover, og heis opp et trinn om n�dvendig
	public void drive(){
		driveForward();													//Kj�r fremover
		while (true){													//Til roboten skrus av:
			if (touching()){											//Hvis vi treffer veggen
				backAfterHittingWall();									//Kj�r litt bakover
				front.firstUp();										//Heis opp fremre heis
				bodyNextToWall();										//Kj�r inntil veggen med hovedplattformen
				elevateBody();											//Heis opp hovedplattformen
				driveWithBodyInAir();									//Kj�r inntil veggen
				elevatorBackUp();										//Heis opp bakre heis
				driveForward();											//Kj�r forover
			}
		}
	}
}