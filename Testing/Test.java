import lejos.hardware.motor.*;							//Motorstyring
import lejos.hardware.port.Port;						//Lesing av sensorporter
import lejos.hardware.Brick;							//Basiskommunikasjon med EV3-"brikken"
import lejos.hardware.BrickFinder;						//Finn EV3-"Brikken"

class Test{


	Brick brick = BrickFinder.getDefault();				//Finn EV3-brikken vi skal bruke

	final int frontElevatorHeight = 10; 				//10 cm h�yde fra gulv til midten av avstandssensor
	final double cmPerSecondElevator = 2.5;				//Med hastighet 100 kj�rer heisen 3 cm per sekund. Det betyr at vi m� kj�re i 3333 ms etter at sensoren m�ler en trapp.

	public Test(){
		System.out.println("Test starting");
	}

	static private void testElevatorSpeed() throws Exception{
		Motor.A.setSpeed(100);
		Motor.A.backward();
		Thread.sleep(3333);
		Motor.A.stop();
	}



	static private void liftFront() throws Exception{
		Motor.A.setSpeed(100);
		Motor.A.backward();
		Thread.sleep(6000);
		Motor.A.setSpeed(1);
		Motor.A.backward();
	}

	static private void liftAll() throws Exception{
		Motor.A.setSpeed(100);
		Motor.B.setSpeed(100);
		Motor.A.forward();
		Motor.B.forward();
		Thread.sleep(3650);
		Motor.A.setSpeed(10);
		Motor.B.setSpeed(10);
	}

	static private void frontInAir() throws Exception{
		Motor.C.setSpeed(150);
		Motor.C.backward();
		Motor.D.setSpeed(150);
		Motor.D.forward();
		Thread.sleep(1000);
		Motor.C.setSpeed(15);
	}

	static private void backUp() throws Exception{
		Motor.C.forward();
		Thread.sleep(3650);
		Motor.C.stop();
	}

	public static void main(String[] args) throws Exception{
		System.out.println("Starting");
		Motor.C.setSpeed(100);
		Motor.D.setSpeed(100);
		Motor.C.forward();
		Thread.sleep(300);
		Motor.D.forward();
		System.out.println("Going up");
		Thread.sleep(5000);
		Motor.C.setSpeed(1);
		Motor.D.setSpeed(1);
		System.out.println("Speed 1 to halt");
		while(true){}
	}
}